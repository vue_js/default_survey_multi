import Vue from 'vue'
import App from './App.vue';
import Survey from "./Survey.vue";

Vue.config.productionTip = false

var location = function () {
    return window.location.href;
};

if(location()==="http://localhost:8080/") {
    new Vue({
        render: h => h(App),
    }).$mount('#app');
}

if(location()==="http://localhost:8080/survey") {
    new Vue({
        render: h => h(Survey),
    }).$mount('#survey');
}
